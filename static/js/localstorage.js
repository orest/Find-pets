const verify = () => {
  fetch(`siteurl/login`, {
    method: 'POST',
    body: JSON.stringify({ login: document.getElementById('login_form'), password: document.getElementById('password_form')})
  })
    .then((user) => {
      localStorage.setValue({
        token: user._id
      })
    })
    .catch((e) => {
      console.error('Bad credentials..');
      alert('Bad login or password');
    })
}
