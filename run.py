import sqlite3,os
from flask import Flask,render_template,request,session,redirect, url_for

app = Flask(__name__)
app.debug = True
app.secret_key = os.urandom(24)
toAdd = False

UPLOAD_FOLDER = 'uploads'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER



@app.route('/')
def default():
    if not session.get('username'):
        return render_template('main.html')
    else:
        return render_template('main_logged.html')

@app.route('/sign_in')
def sign_in():
    if not session.get('username'):
        return render_template('sign_in.html')
    else:
        return render_template('main.html')

@app.route('/admission', methods = ['POST','GET'])
def admission():
    if request.method == 'POST':
        login = request.form['login']
        password = request.form['pin']
        db = sqlite3.connect('database.db')
        cursor = db.cursor()
        cursor.execute('SELECT password FROM users_pets WHERE login = (?)',(login,))
        password_check = cursor.fetchone()
        if password_check != None:
            i = 0
            while(i<len(password_check)):
                if password == password_check[i]:
                    cursor.execute('SELECT id FROM users_pets WHERE login =(?)',(login,))
                    id1 = cursor.fetchone()
                    session['username'] = True
                    session['login'] = login
                    if(toAdd==True):
                        return render_template('add_pet.html')
                    else:
                        return render_template('main_logged.html')
                else:
                    return render_template('sign_in.html', povidomlenya = "Wrong password. Try again.", )

        else:
            return render_template('sign_in.html', povidomlenya = "User with this login doesn't exist. Try again.", )

        db.close()


@app.route('/sign_up')
def sign_up():
   return render_template('sign_up.html')

@app.route('/data_users', methods=['POST', 'GET'])
def data_users():
    if request.method == 'POST' or request.method =='GET':
        try:
            login = request.form['login']
            name = request.form['nm']
            surname = request.form['surname']
            city = request.form['city']
            email = request.form['email']
            password = request.form['pin']
            phone = request.form['phone']
            neighborhood = request.form['neighborhood']
            with sqlite3.connect("database.db") as db:
                cursor = db.cursor()
                cursor.execute("SELECT login FROM users_pets WHERE login =(?)",(str(login) ,))
                user_is = cursor.fetchone()
                if user_is == None and len(name) >=1 and len(surname) >=1 and len(password) >= 1 and len(login) >= 1:
                    cursor.execute("INSERT INTO users_pets (login,name,surname,city,email,phone,neighborhood,password)VALUES( ?, ?, ?, ?, ?, ?, ?, ?)",(login,name,surname,city,email,phone,neighborhood,password))
                    db.commit()
                    povidomlenya = "User was successfuly added to our database"
                else:
                    povidomlenya = 'Fail. Fill up all the forms'
        except:
            db.rollback()
            povidomlenya = "ERORR 404"
        finally:
            return render_template("result.html", povidomlenya = povidomlenya,name = name)
            db.close()

@app.route('/add_pet')
def add_pet():
    if not session.get('username'):
        toAdd = True
        return render_template('sign_in.html', povidomlenya = "You need to sign in first in order to add a pet.", )
    else:
        return render_template('add_pet.html')

@app.route('/data_pets', methods=['GET', 'POST'])
def data_pets():
    if request.method == 'POST':
        try:
            login_user = session.get('login')
            status = request.form['status']
            pet_type = request.form['pet_type']
            city = request.form['city']
            neighborhood = request.form['neighborhood']
            date = request.form['date']
            description = request.form['description']
            file = request.files['inputFile']
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))
            image = file.filename

            with sqlite3.connect('database.db') as db:
                cursor = db.cursor()
                cursor.execute('INSERT INTO pets (login_user,status,type,city,neighborhood,date,description,image) VALUES(?,?,?,?,?,?,?,?)',(login_user,status,pet_type,city,neighborhood,date,description,image))
                db.commit()
                povidomlenya ='Your pet was successfuly added to our database'
        except:
            db.rollback()
            povidomlenya = "ERORR 404"
        finally:
            db.close()
            return render_template('result.html', povidomlenya = povidomlenya)


@app.route('/sign_out')
def sign_out():
    session.pop('login', None)
    session['username'] = False
    return render_template('main.html')


@app.route('/find_pet')
def find_pet():
    if not session.get('username'):
        return render_template('sign_in.html', povidomlenya = "You need to sign in first in order to search for a pet.", )
    else:
        return render_template('find_pet_logged.html')

@app.route('/i_lost_pet')
def i_lost_pet():
    if not session.get('username'):
        return render_template('sign_in.html', povidomlenya = "You need to sign in first in order to search for a pet.", )
    else:
        return render_template('i_lost_pet_logged.html')

@app.route('/i_found_pet')
def i_found_pet():
    if not session.get('username'):
        return render_template('sign_in.html', povidomlenya = "You need to sign in first in order to search for a pet.", )
    else:
        return render_template("i_found_pet_logged.html")

@app.route('/i_want_pet')
def i_want_pet():
    if not session.get('username'):
        return render_template('sign_in.html', povidomlenya = "You need to sign in first in order to search for a pet.", )
    else:
        return render_template("i_want_pet_logged.html")

@app.route('/pretenders',methods = ['POST','GET'])
def pretenders():
    if request.method == 'POST':
        status = request.form.get('status', None)
        type = request.form.get('type', None)
        city = request.form.get('city', None)
        neighborhood = request.form.get('neighborhood', None)
        with sqlite3.connect('database.db') as db:
            cursor = db.cursor()
            cursor.execute('SELECT * FROM pets WHERE status = (?) and type = (?) and city = (?) and neighborhood = (?)',(status,type,city,neighborhood))
            pretenders = cursor.fetchall()

        if not pretenders:
            if(status=="lost"):
                return render_template("search_result_lost.html", povidomlenya = "Sorry, there are no pets like this in our database")
            if(status=="found"):
                return render_template("search_result_found.html", povidomlenya = "Sorry, there are no pets like this in our database")
            if(status=="sell"):
                return render_template("search_result_sell.html", povidomlenya = "Sorry, there are no pets like this in our database")
        else:
            if (status=="lost"):
                return render_template("search_result_lost.html", pretenders = pretenders)
            if (status=="found"):
                return render_template("search_result_found.html", pretenders = pretenders)
            if (status=="sell"):
                return render_template("search_result_sell.html", pretenders = pretenders)

        db.close()


@app.route('/show_all_lost',methods = ['POST','GET'])
def show_all_lost():
    with sqlite3.connect('database.db') as db:
        cursor = db.cursor()
        cursor.execute('SELECT * FROM pets WHERE status = "lost"')
        pretenders = cursor.fetchall()
        if pretenders:
            return render_template("search_result.html", pretenders = pretenders, show_all = "show_all_lost")
        else:
            return "Nema takih psiv"
        db.close()


@app.route('/show_all_found',methods = ['POST','GET'])
def show_all_found():
    with sqlite3.connect('database.db') as db:
        show_all = "show_all_found"
        cursor = db.cursor()
        cursor.execute('SELECT * FROM pets WHERE status = "found"')
        pretenders = cursor.fetchall()
        if pretenders:
            return render_template("search_result.html", pretenders = pretenders, show_all = show_all)
        else:
            return "Nema takih psiv"
        db.close()
@app.route('/show_all_sell',methods = ['POST','GET'])
def show_all_sell():
    with sqlite3.connect('database.db') as db:
        show_all = "show_all_sell"
        cursor = db.cursor()
        cursor.execute('SELECT * FROM pets WHERE status = "sell"')
        pretenders = cursor.fetchall()
        if pretenders:
            return render_template("search_result.html", pretenders = pretenders, show_all = show_all)
        else:
            return "Nema takih psiv"
        db.close()


if __name__ == "__main__":
    app.run(port = 5000, debug=True)
